

Caly : "calcule et lis" is a software designed to teach young kids 
       to learn numbers as well as to learn how to read in french.

Usage: 
  caly

Keys: 
  "m"           : change mode (numbers or words)
  "space"       : next number of next word
  "+"           : next level
  "-"           : previous level
  "q" or "Esc"  : quit





Additional scripts

./convert_dic2ascii.py fr_CH.dic 4    > words_01.txt
./convert_dic2ascii.py fr_CH.dic 5    > words_02.txt
./convert_dic2ascii.py fr_CH.dic 1000 > words_03.txt



