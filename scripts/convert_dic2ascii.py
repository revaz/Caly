#!/usr/bin/env python3

import sys

fle1 = sys.argv[1]
nmax = int(sys.argv[2])

f = open(fle1,"r")
lines = f.readlines()
f.close()

words = []

for line in lines:
  
  line = line.strip()
  
  line = line.split('/')[0]   
  
  if len(line) <= nmax:
  
    print(line)

