
import os

DIR = os.path.dirname(__file__)
CALYDIR = os.path.dirname(DIR)
CONFDIR = os.path.join(CALYDIR,"conf")
TXTDIR  = os.path.join(CONFDIR,"txt")
SNDDIR  = os.path.join(CONFDIR,"sounds")
